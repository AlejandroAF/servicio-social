<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class carreras extends Model
{
    use HasFactory;

    protected $table = 'carreras';
    protected $primaryKey = 'id';
    public $incrementing = true;

    static $rules = [
        'nombre_carrera' => 'required|string|max:255'
    ];

    protected $fillable = [
        'nombre_carrera',
    ];


    public $timestamps = true;
}
