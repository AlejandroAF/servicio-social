<?php

namespace App\Http\Controllers;

use App\Models\SolicitudExternaServidores;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SolicitudExternaServidoresController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $validated = $request->validate([
            'nombre_dependencia' => 'required',
            'domicilio' => 'required',
            'telefono' => 'required|numeric',
            'responsable_dependencia' => 'required',
            'correo_dependencia' => 'required',
            'responsable_directo_programa' => 'required',
            'correo_responsable_directo' => 'required',
            'area_departamento' => 'required',
            'nombre_dependencia' => 'required',
            'oficina_seccion' => 'required',
            'nombre_programa' => 'required',
            'impacto_social' => 'required',
            'objetivo' => 'required',
            'compensacion_economica' => 'required',
            'numero_beneficiarios_directos' => 'required|numeric',
            'numero_beneficiarios_indirectos' => 'required|numeric',
            'fecha_inicio_programa' => 'required',
            'fecha_termino_programa' => 'required',
        ]);

        SolicitudExternaServidores::create([
            'nombre_dependencia' => $request->nombre_dependencia,
            'domicilio' => $request->domicilio,
            'telefono' => $request->telefono,
            'responsable_dependencia' => $request->responsable_dependencia,
            'correo_dependencia' => $request->correo_dependencia,
            'responsable_directo_programa' => $request->responsable_directo_programa,
            'correo_responsable_directo' => $request->correo_responsable_directo,
            'area_departamento' => $request->area_departamento,
            'oficina_seccion' => $request->oficina_seccion,
            'nombre_programa' => $request->nombre_programa,
            'impacto_social' => $request->impacto_social,
            'objetivo' => $request->objetivo,
            'compensacion_economica' => $request->compensacion_economica,
            'numero_beneficiarios_directos' => $request->numero_beneficiarios_directos,
            'numero_beneficiarios_indirectos' => $request->numero_beneficiarios_indirectos,
            'fecha_solicitud' => now()->toDateString(),
            'fecha_inicio_programa' => $request->fecha_inicio_programa,
            'fecha_termino_programa' => $request->fecha_termino_programa,
            'folio_asignado' => "0",
            'confirmacion' => false,
        ]);

        //return Redirect::route("/solicitud-externa-servidores");

    }

    /**
     * Display the specified resource.
     */
    public function show(SolicitudExternaServidores $solicitudExternaServidores)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(SolicitudExternaServidores $solicitudExternaServidores)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, SolicitudExternaServidores $solicitudExternaServidores)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(SolicitudExternaServidores $solicitudExternaServidores)
    {
        //
    }
}
